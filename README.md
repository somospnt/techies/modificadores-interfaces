# Descripción del Proyecto

Este proyecto es un ejercicio práctico que tiene como objetivo trabajar el uso de modificadores de acceso, clases abstractas e interfaces en java.

# Para ejecutar el proyecto

Clonar este repositorio:

``` https://gitlab.com/somospnt/techies/modificadores-interfaces.git ```


# Ejercicio 

Teniendo en cuenta los modificadores de acceso analizados:

- Crear una clase abstracta llamada Persona con los atributos de: nombre y edad. Además, debe tener un método abstracto llamado describir(). 

- Crear la clase Alumno con atributo id y heredar de la clase abstracta Persona los atributos de edad y nombre. 

- Crea una interfaz llamada Estudiante teniendo en cuenta que debe tener un método llamado estudiar().

- Modificar la clase Alumno para que herede de Persona los métodos estudiar y describir e implemente la interfaz Estudiante. El método estudiar debe tener un println("Estudiando...") y el método describir debe tener un printIn que concatena los datos del alumno (id, nombre, edad).

- Por último, llamar a los métodos desde la clase techie que contiene el método main y ejecutar el run para visualizar por consola el resultado obtenido.
